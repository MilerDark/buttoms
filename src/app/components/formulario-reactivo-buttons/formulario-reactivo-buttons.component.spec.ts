import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioReactivoButtonsComponent } from './formulario-reactivo-buttons.component';

describe('FormularioReactivoButtonsComponent', () => {
  let component: FormularioReactivoButtonsComponent;
  let fixture: ComponentFixture<FormularioReactivoButtonsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormularioReactivoButtonsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioReactivoButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
